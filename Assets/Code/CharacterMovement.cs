﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour {
    public static CharacterMovement instance;
    private void Awake()
    {
        instance = this;
    }

    [SerializeField] bool canMove;
    [SerializeField] float movementSpeed;
    Vector2 temppos, inpt;

    SpriteRenderer charRenderer;
    public AnimationThingy down, left, up, right;
    [SerializeField] float animationPause;

    [System.Serializable]
    public class AnimationThingy
    {   
        public Sprite stand;
        public Sprite[] walkAnim;
    }

    public void Start()
    {
        charRenderer = transform.GetComponent<SpriteRenderer>();
        StartCoroutine(animInt());
        cur = down;
    }

    private void Update()
    {
        inpt.x = Input.GetAxisRaw("Horizontal");
        inpt.y = Input.GetAxisRaw("Vertical");
        animationBullshit(inpt.x, inpt.y);
        if (canMove)
        {
            transform.position = transform.position + (Vector3)(inpt * movementSpeed * Time.deltaTime);
        }
    }

    AnimationThingy cur;
    void animationBullshit(float x, float y)
    {
        if (x != 0 || y != 0)
        {
            if (Mathf.Abs(x) > Mathf.Abs(y))
            {
                if (x > 0)
                {
                    cur = right;
                }
                else
                {
                    cur = left;
                }
            }
            else
            {
                if (y > 0)
                {
                    cur = up;
                }
                else
                {
                    cur = down;
                }
            }
            if (cur.walkAnim.Length != 0)
            {
                charRenderer.sprite = cur.walkAnim[AnInt % cur.walkAnim.Length];
            }
        }
        else
        {
            charRenderer.sprite = cur.stand;
        }
    }

    int AnInt;
    IEnumerator animInt()
    { 
        while (true)
        {
            AnInt++;
            yield return new WaitForSeconds(animationPause);
        }
    }
}
