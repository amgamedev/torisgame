﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueUI : MonoBehaviour
{
    [SerializeField] Text text1, text2;

    [SerializeField] int charactersPerSecond = 12;

    string cur;

    public string DebugText;
    Coroutine debugCRT;

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.F1))
        {
            if (debugCRT != null) {
                StopCoroutine(debugCRT);
            }
            debugCRT = StartCoroutine(PlaceLetters(DebugText));
        }
    }

    IEnumerator PlaceLetters(string temp)
    {
        text1.text = "";
        text2.text = "";
        cur = "";
        for(int i = 0; i < temp.Length; i++)
        {
            yield return new WaitForSeconds(1 / (float)charactersPerSecond);
            cur = cur + temp[i].ToString();
            text1.text = cur;
            text2.text = cur;
        }
        yield return null;
    }

}
