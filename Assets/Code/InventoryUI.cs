﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : MonoBehaviour {
    public RectTransform InventoryObject;
    [SerializeField] RectTransform innerAnchor, outterAnchor;
    [SerializeField] float transitionDuration;

    public AnimationCurve enterExitCurve;

    Coroutine cur;
    float targettime;
    Vector2 pos;

    bool isOpen, canswitch = true;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            if (canswitch)
            {
                if (isOpen)
                {
                    StartCoroutine(Inventory(outterAnchor.anchoredPosition, innerAnchor.anchoredPosition));
                }
                else
                {
                    StartCoroutine(Inventory(innerAnchor.anchoredPosition, outterAnchor.anchoredPosition));
                }
            }
        }
    }
    float perrrrcent;
    IEnumerator Inventory(Vector2 targetAnchor, Vector2 originAnchor)
    {
        canswitch = false;
        targettime = Time.time + transitionDuration;
        pos = outterAnchor.anchoredPosition;
        while (Time.time < targettime)
        {
            perrrrcent = (targettime - Time.time) / transitionDuration;
            
            perrrrcent = enterExitCurve.Evaluate(perrrrcent);
            
            InventoryObject.anchoredPosition = returnAnchor(perrrrcent, originAnchor, targetAnchor);
            yield return null;
        }
        InventoryObject.anchoredPosition = targetAnchor;
        yield return null;
        canswitch = true;
        isOpen = !isOpen;
    }
    Vector2 temp;
    Vector2Int returnAnchor(float pcent, Vector2 innerAnchor, Vector2 outteranchor)
    {
        temp = (outteranchor + ((innerAnchor - outteranchor) * pcent));
        return Vector2Int.RoundToInt(temp);
    }
}
